#{
#  pkgs ? import <nixpkgs> { },
#}:

let

  nixpkgs = builtins.fetchTarball {
    url = "https://github.com/NixOS/nixpkgs/archive/2057814051972fa1453ddfb0d98badbea9b83c06.tar.gz";
    sha256 = "1a8jafyawg8mysv787q0cwghkza9kahbbniism7v2rcxas89b575";
  };
  pkgs = import nixpkgs { };

  inherit (pkgs)
    lib
    buildNimPackage
    pkg-config
    tdlib
    ;
in

buildNimPackage {
  pname = "telegram_actor";
  version = "unstable";
  src = if lib.inNixShell then null else lib.cleanSource ./.;
  buildInputs = [ tdlib ];
  nativeBuildInputs = [ pkg-config ];
  lockFile = ./lock.json;
}
