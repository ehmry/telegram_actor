version = "20240522"
author = "Emery Hemingway"
description = "Telegram client as a Syndicated Actor"
license = "Unlicense"
srcDir = "src"
bin = @["telegram_actor"]

requires "https://git.syndicate-lang.org/ehmry/syndicate-nim.git >= 20240522"
