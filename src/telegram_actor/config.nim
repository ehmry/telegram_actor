
import
  preserves

type
  TDLibClient* {.preservesRecord: "tdlib".} = object
    `messages`* {.preservesEmbedded.}: Value

  TelegramReady* {.preservesRecord: "telegram-ready".} = object
  
  TelegramArgumentsField0* {.preservesDictionary.} = object
    `dataspace`* {.preservesEmbedded.}: Value

  TelegramArguments* {.preservesRecord: "telegram-client".} = object
    `field0`*: TelegramArgumentsField0

proc `$`*(x: TDLibClient | TelegramReady | TelegramArguments): string =
  `$`(toPreserves(x))

proc encode*(x: TDLibClient | TelegramReady | TelegramArguments): seq[byte] =
  encode(toPreserves(x))
