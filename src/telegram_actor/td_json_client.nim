# SPDX-FileCopyrightText: ☭ Emery Hemingway
# SPDX-License-Identifier: Unlicense

{.passC: staticExec("pkg-config --cflags tdjson").}
{.passL: staticExec("pkg-config --libs tdjson").}

{.pragma: import_td, importc, header: "<td/telegram/td_json_client.h>".}

type
  ClientId* = distinct cint
  Client* = distinct pointer
  LogMessageCallback* = proc (verbosity_level: cint; message: cstring) {.cdecl.}

proc td_create_client_id*(): ClientId {.import_td.}

proc td_send*(client_id: ClientId; request: cstring) {.import_td.}

proc td_receive*(timeout: cdouble): cstring {.import_td.}

proc td_execute*(request: cstring): cstring {.import_td.}

proc td_set_log_message_callback*(max_verbosity_level: cint; callback: LogMessageCallback) {.import_td.}

proc td_json_client_create*(): Client {.import_td, deprecated.}

proc td_json_client_send*(client: Client; request: cstring) {.import_td, deprecated.}

proc td_json_client_receive*(client: Client; timeout: cdouble): cstring {.import_td, deprecated.}

proc td_json_client_execute*(client: Client; request: cstring): cstring {.import_td, deprecated.}

proc td_json_client_destroy*(client: Client) {.import_td, deprecated.}
