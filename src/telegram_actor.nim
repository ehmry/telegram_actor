# SPDX-FileCopyrightText: ☭ Emery Hemingway
# SPDX-License-Identifier: Unlicense

import
  std/[options, streams, tables, times],
  preserves, syndicate, syndicate/relays,
  ./telegram_actor/[config, td_json_client]

discard td_execute("""{"@type":"setLogVerbosityLevel", "new_verbosity_level":0}""")
  # Disable log vomit to stderr, this isn't a GTK application.

type State = ref object
  facet: Facet
  initialRef: Cap
  subscribers: Table[int, Cap]
  buffer: StringStream
  recvError: Handle

let state = State( buffer: newStringStream() )
  # Global state object

let actor = bootActor("main") do (turn: Turn):
  state.facet = turn.facet
  resolveEnvironment(turn) do (turn: Turn; ds: Cap):
    state.initialRef = ds

    during(turn, ds, TelegramArguments.grabTypeFlat) do (ds: Cap):

      # Extern actors assert themselves here and a ClientId
      # is used to isolate their conversations with tdlib.
      let
        client =  td_create_client_id()
        tdlib = turn.newDataspace()
      state.subscribers[client.int] = tdlib

      onMessage(turn, tdlib, grabRecord("send", grab())) do (v: Value):
        state.buffer.setPosition 0
        state.buffer.writeText(v, textJson)
        state.buffer.data.setLen state.buffer.getPosition
        td_send(client, state.buffer.data)
          # TODO: enforce that the subscriber has not set "@client_id"?

      publish(turn, ds, TDLibClient(messages: tdlib.embed))

    do:
      state.subscribers.del client.int

proc recv(state: State; id: int; recvMsg: Value) =
  let subscriber = state.subscribers.getOrDefault id
  if not subscriber.isNil:
    queueTurn(state.facet) do (turn: Turn):
      message(turn, subscriber, recvMsg)

proc recv(state: State; cstr: cstring) =
  try:
    var
      pr = parsePreserves $cstr
      clientId: Value
    if pr.pop("@client_id".toPreserves, clientId) and clientId.isInteger:
      # Pop the client_id out of messages so it isn't exposed to the subscriber.
      recv(state, clientId.register, initRecord("recv", pr))
    else:
      stderr.writeLine "discarding message without @client_id: ", cstr
  except ValueError:
    var buf = newSeq[byte](cstr.len)
    copyMem(buf[0].addr, cstr[0].addr, buf.len)
    var rec = initRecord("telegram-recv-error", buf.toPreserves)
    stderr.writeLine rec
    queueTurn(state.facet) do (turn: Turn):
      replace(turn, state.initialRef, state.recvError, rec)

while actor.running:
  # Poll Syndicate and Telegram
  const
    syndicateTimeout = some initDuration(seconds = 4)
    telegramTimeout = 4.0
  syndicate.runOnce(syndicateTimeout)
  let cstr = td_receive(telegramTimeout)
  if not cstr.isNil:
    recv(state, cstr)
