# telegram_actor

A proxy for communicating with [TDLib](https://core.telegram.org/tdlib/docs/td__json__client_8h.html) over Syndicate using the JSON-serialized object API.

## Do not use Telegram.

Probably a front for gathering metadata, like Signal, except FSB.

## Example config
```
#!/usr/bin/env -S syndicate-server --config

<require-service <daemon telegram_actor>>

<daemon telegram_actor {
  argv: [ "/bin/telegram_actor" ]
  restart: never
  protocol: application/syndicate
}>

let ?ds = dataspace

? <service-object <daemon telegram_actor> ?cap> [
  $cap ? <telegram-recv-error ?err> [ $log ! <log "-" { telegram-recv-error: $err }> ]
  $cap <telegram-client { dataspace: $ds }>
]

$ds [
  ? ?v [
    $log ! <log "-" { |+++|: $v }>
    ?- $log ! <log "-" { |---|: $v }>
  ]

  # Raw TDLib interaction.
  ? <tdlib ?messages> $messages [
    ?? <send ?v> [ $log ! <log "-" { |>>>|: $v }> ]
    ?? <recv ?v> [ $log ! <log "-" { |<<<|: $v }> ]
    ?? <recv {"@type": "error", "code": ?code, "message": ?message}> [
      $log ! <log "-" { error: {code: $code, message: $message } }>
    ]

    ! <send {"@type": "getOption", "name":"version"}>

    ?? <recv {"@type": "updateAuthorizationState", "authorization_state": {"@type": "authorizationStateWaitTdlibParameters"}}> [
      ! <send {
        "@type":"setTdlibParameters"
        "database_directory":"/home/cache/tdlib"
        "use_message_database":true
        "use_secret_chats":true
        "api_id": $api_id
        "api_hash": $api_hash
        "system_language_code":"en-GB"
        "device_model":"iPhone Z"
        "application_version":"1.0"
      }>
    ]

    ?? <recv {"@type": "updateAuthorizationState", "authorization_state": {"@type": "authorizationStateWaitPhoneNumber"}}> [
      ! <send {
        "@type":"setAuthenticationPhoneNumber"
        "phone_number": $phone_number
      }>
    ]

    ?? <recv {"@type": "updateAuthorizationState", "authorization_state": {"@type": "authorizationStateWaitCode"}}> [
      ! <send {
        "@type":"checkAuthenticationCode"
        "code": $login_code
      }>
    ]

    ?? <recv {"@type": "updateAuthorizationState", "authorization_state": {"@type": "authorizationStateReady"}}> [
      ! <send {
        "@type": "loadChats"
        "limit": 32
      }>
    ]

    ?? <recv {"@type": "updateNewChat", "id": ?id,"chat": {"title": ?title}}> [
      $ds <chat $id $title>
    ]

    ?? <recv {
        "@type": "updateUser"
        "user": {
          "id": ?id
          "first_name": ?first_name
          "last_name": ?last_name
        }
      }> [
      $ds <user $id $first_name $last_name>
    ]

    ?? <recv {
        "@type": "updateChatLastMessage"
        "chat_id": ?chat_id
        "last_message": {
          "content": {
            "text": {
              "text": ?text
            }
          }
          "date": ?date
          "sender_id": {
            "user_id": ?user_id
          }
        }
      }> [
        $log ! <log "-" { updateChatLastMessage: { chat_id: $chat_id, date: $date, text: $text, user_id: $user_id } }>
    ]

  ]
]
```
